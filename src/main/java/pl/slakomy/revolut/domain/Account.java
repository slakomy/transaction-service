package pl.slakomy.revolut.domain;

import org.hibernate.annotations.Columns;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.jadira.usertype.moneyandcurrency.moneta.PersistentMoneyAmountAndCurrency;

import javax.money.CurrencyUnit;
import javax.money.MonetaryAmount;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ACCOUNTS")
@TypeDef(name = "moneyAmountWithCurrency", typeClass = PersistentMoneyAmountAndCurrency.class)
public class Account {
    @Id
    private Long number;

    @Columns(columns = {@Column(name = "CURRENCY"), @Column(name = "AMOUNT")})
    @Type(type = "moneyAmountWithCurrency")
    private MonetaryAmount bankBalance;

    @SuppressWarnings("unused") // for hibernate
    private Account() {}

    public Account(Long number, MonetaryAmount bankBalance) {
        this.number = number;
        this.bankBalance = bankBalance;
    }

    public Long getNumber() {
        return number;
    }

    public MonetaryAmount getBankBalance() {
        return bankBalance;
    }

    public CurrencyUnit getCurrency() {
        return bankBalance.getCurrency();
    }

    public boolean withdraw(MonetaryAmount amount) {
        if (bankBalance.isGreaterThanOrEqualTo(amount)) {
            bankBalance = bankBalance.subtract(amount);
            return true;
        }
        return false;
    }

    public void ante(MonetaryAmount amount) {
        bankBalance = bankBalance.add(amount);
        bankBalance.getNumber();
    }
}
