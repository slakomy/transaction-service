package pl.slakomy.revolut.domain;

import java.math.BigDecimal;

public class Transfer {
    private Long recipientAccountNumber;
    private Long senderAccountNumber;
    private BigDecimal amount;

    public Transfer(Long recipientAccountNumber, Long senderAccountNumber, BigDecimal amount) {
        this.recipientAccountNumber = recipientAccountNumber;
        this.senderAccountNumber = senderAccountNumber;
        this.amount = amount;
    }

    public Long getRecipientAccountNumber() {
        return recipientAccountNumber;
    }

    public Long getSenderAccountNumber() {
        return senderAccountNumber;
    }

    public BigDecimal getAmount() {
        return amount;
    }

}
