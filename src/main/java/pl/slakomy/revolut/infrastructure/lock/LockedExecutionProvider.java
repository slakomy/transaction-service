package pl.slakomy.revolut.infrastructure.lock;

import javax.inject.Inject;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import static java.util.Arrays.asList;

public class LockedExecutionProvider {
    private final LocksPool lockPool;

    @Inject
    public LockedExecutionProvider(LocksPool lockPool) {
        this.lockPool = lockPool;
    }

    public <T> T executeLocked(Supplier<T> supplier, Long... accountsIds) {
        List<Lock> idsLocks = Arrays.stream(accountsIds).sorted().map(lockPool::getLock).collect(Collectors.toList());
        idsLocks.forEach(Lock::lock);
        try {
            return supplier.get();
        } finally {
            idsLocks.forEach(Lock::unlock);
            asList(accountsIds).forEach(lockPool::markUsageComplete);
        }
    }

}
