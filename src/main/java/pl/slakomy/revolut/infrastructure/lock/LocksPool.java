package pl.slakomy.revolut.infrastructure.lock;

import com.google.common.annotations.VisibleForTesting;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.Lock;

class LocksPool {
    private final Map<Long, UsageTrackedLock> locks = new HashMap<>();

    synchronized Lock getLock(Long id) {
        UsageTrackedLock UsageTrackedLock = locks.computeIfAbsent(id, i -> new UsageTrackedLock());
        UsageTrackedLock.incrementUsageCount();
        return UsageTrackedLock.getLock();
    }

    synchronized void markUsageComplete(Long id) {
        UsageTrackedLock UsageTrackedLock = locks.get(id);
        if (0 == UsageTrackedLock.decrementUsageCount()) {
            locks.remove(id);
        }
    }

    @VisibleForTesting
    Map<Long, UsageTrackedLock> getLocks() {
        return locks;
    }
}
