package pl.slakomy.revolut.infrastructure.lock;

import com.google.common.annotations.VisibleForTesting;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

class UsageTrackedLock {
    private final Lock lock;
    private int usageCount;

    UsageTrackedLock() {
        this.lock = new ReentrantLock();
        usageCount = 0;
    }

    Lock getLock() {
        return lock;
    }

    void incrementUsageCount() {
        usageCount++;
    }

    int decrementUsageCount() {
        return --usageCount;
    }

    @VisibleForTesting
    int getUsageCount() {
        return usageCount;
    }
}
