package pl.slakomy.revolut.infrastructure;

import pl.slakomy.revolut.domain.Account;

import javax.inject.Inject;
import javax.inject.Provider;
import javax.persistence.EntityManager;

public class AccountRepository {
    private final Provider<EntityManager> entityManagerProvider;

    @Inject
    public AccountRepository(Provider<EntityManager> emProvider) {
        this.entityManagerProvider = emProvider;
    }

    public Account findBy(Long accountNumber) {
        return entityManagerProvider.get().find(Account.class, accountNumber);
    }

    public void save(Account account) {
        entityManagerProvider.get().persist(account);
    }

    public void update(Account... accounts) {
        EntityManager entityManager = entityManagerProvider.get();
        for (Account account : accounts) {
            entityManager.merge(account);
        }
    }
}
