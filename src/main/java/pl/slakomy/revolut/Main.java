package pl.slakomy.revolut;

import com.google.inject.Binder;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Module;
import com.google.inject.persist.PersistService;
import com.google.inject.persist.jpa.JpaPersistModule;
import org.javamoney.moneta.internal.convert.ECBCurrentRateProvider;
import pl.slakomy.revolut.application.TransferController;
import ro.pippo.controller.ControllerApplication;
import ro.pippo.core.Pippo;
import ro.pippo.guice.GuiceControllerFactory;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.money.convert.ExchangeRateProvider;

public class Main extends ControllerApplication {

    public static void main(String[] args) {
        new Pippo(new Main()).start();
    }

    @Override
    protected void onInit() {
        Injector injector = Guice.createInjector(new PersistenceModule(), new TransferModule());
        setControllerFactory(new GuiceControllerFactory(injector));
        addControllers(injector.getInstance(TransferController.class));
    }

    private static final class PersistenceModule implements Module {
        @Override
        public void configure(Binder binder) {
            binder.install(new JpaPersistModule("account-pu"));
            binder.bind(PersistenceInitializer.class).asEagerSingleton();
        }
    }

    @Singleton
    private static final class PersistenceInitializer {
        @Inject
        public PersistenceInitializer(PersistService service) {
            service.start();
        }
    }

    private static final class TransferModule implements Module {
        @Override
        public void configure(Binder binder) {
            binder.bind(ExchangeRateProvider.class).to(ECBCurrentRateProvider.class);
        }
    }
}
