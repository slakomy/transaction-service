package pl.slakomy.revolut.application;

import javax.inject.Inject;
import javax.money.CurrencyUnit;
import javax.money.MonetaryAmount;
import javax.money.convert.CurrencyConversion;
import javax.money.convert.ExchangeRateProvider;

public class ExchangeService {
    private final ExchangeRateProvider exchangeRateProvider;

    @Inject
    public ExchangeService(ExchangeRateProvider exchangeRateProvider) {
        this.exchangeRateProvider = exchangeRateProvider;
    }

    public MonetaryAmount exchange(MonetaryAmount monetaryAmount, CurrencyUnit targetCurrency) {
        if (monetaryAmount.getCurrency().equals(targetCurrency)) {
            return monetaryAmount;
        }
        CurrencyConversion currencyConversion = exchangeRateProvider.getCurrencyConversion(targetCurrency);
        return monetaryAmount.with(currencyConversion);
    }
}
