package pl.slakomy.revolut.application;

import com.google.inject.persist.Transactional;
import pl.slakomy.revolut.domain.Account;
import pl.slakomy.revolut.domain.Transfer;
import pl.slakomy.revolut.infrastructure.AccountRepository;

import javax.inject.Inject;
import javax.money.MonetaryAmount;
import java.math.BigDecimal;

import static javax.money.Monetary.getDefaultAmountFactory;

public class TransferService {
    private final AccountRepository accountRepository;
    private final ExchangeService exchangeService;

    @Inject
    public TransferService(AccountRepository accountRepository, ExchangeService exchangeService) {
        this.accountRepository = accountRepository;
        this.exchangeService = exchangeService;
    }

    @Transactional
    public boolean make(Transfer transfer) {
        if (isInvalid(transfer.getAmount())) {
            return false;
        }
        Account senderAccount = accountRepository.findBy(transfer.getSenderAccountNumber());
        Account recipientAccount = accountRepository.findBy(transfer.getRecipientAccountNumber());
        if (senderAccount == null || recipientAccount == null) {
            return false;
        }
        MonetaryAmount transferAmount = getDefaultAmountFactory().setNumber(transfer.getAmount()).setCurrency(senderAccount.getCurrency()).create();
        if (senderAccount.withdraw(transferAmount)) {
            recipientAccount.ante(exchangeService.exchange(transferAmount, recipientAccount.getCurrency()));
            accountRepository.update(senderAccount, recipientAccount);
            return true;
        }
        return false;
    }

    private boolean isInvalid(BigDecimal amount) {
        return amount.signum() != 1 || amount.scale() > 2;
    }
}
