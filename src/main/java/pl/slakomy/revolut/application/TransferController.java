package pl.slakomy.revolut.application;

import pl.slakomy.revolut.domain.Transfer;
import pl.slakomy.revolut.infrastructure.lock.LockedExecutionProvider;
import ro.pippo.controller.Consumes;
import ro.pippo.controller.Controller;
import ro.pippo.controller.POST;
import ro.pippo.controller.Path;
import ro.pippo.controller.Produces;

import javax.inject.Inject;

@Path("/transfer")
public class TransferController extends Controller {
    private final TransferService transferService;
    private final LockedExecutionProvider lockedExecutionProvider;

    @Inject
    public TransferController(TransferService transferService, LockedExecutionProvider lockedExecutionProvider) {
        this.transferService = transferService;
        this.lockedExecutionProvider = lockedExecutionProvider;
    }

    @POST
    @Consumes(Consumes.JSON)
    @Produces(Produces.JSON)
    public void transfer() {
        Transfer transfer = getRouteContext().createEntityFromBody(Transfer.class);
        if (!areAllFieldsPresent(transfer)) {
            getRouteContext().getResponse().badRequest();
        } else {
            if (makeInLock(transfer)) {
                getRouteContext().getResponse().ok();
            } else {
                getRouteContext().getResponse().forbidden();
            }
        }
    }

    public boolean makeInLock(Transfer transfer) {
        return lockedExecutionProvider.executeLocked(() -> transferService.make(transfer), transfer.getRecipientAccountNumber(), transfer.getSenderAccountNumber());
    }

    private boolean areAllFieldsPresent(Transfer transfer) {
        return transfer != null && transfer.getSenderAccountNumber() != null
                && transfer.getRecipientAccountNumber() != null && transfer.getAmount() != null;
    }
}