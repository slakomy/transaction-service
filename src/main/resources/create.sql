create table ACCOUNTS (number bigint not null, CURRENCY varchar(255), AMOUNT decimal(19,2), primary key (number));
ALTER TABLE "ACCOUNTS" ADD CONSTRAINT AMOUNT_CHECK CHECK ( AMOUNT >= 0 );