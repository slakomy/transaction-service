package pl.slakomy.revolut.application;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import pl.slakomy.revolut.domain.Account;
import pl.slakomy.revolut.domain.Transfer;
import pl.slakomy.revolut.infrastructure.AccountRepository;

import javax.money.CurrencyUnit;
import javax.money.Monetary;
import javax.money.MonetaryAmount;
import java.math.BigDecimal;

import static javax.money.Monetary.getDefaultAmountFactory;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class TransferServiceTest {
    private static final CurrencyUnit RECIPIENT_CURRENCY = Monetary.getCurrency("USD");
    private static final CurrencyUnit SENDER_CURRENCY = Monetary.getCurrency("PLN");
    private static final Long RECIPIENT_ACCOUNT_NUMBER = 1L;
    private static final Long SENDER_ACCOUNT_NUMBER = 2L;
    private TransferService transferService;
    @Mock
    private ExchangeService exchangeService;
    @Mock
    private AccountRepository accountRepository;
    @Mock
    private Account senderAccount;
    @Mock
    private Account recipientAccount;
    private Transfer transfer;
    @Mock
    private MonetaryAmount senderBalance;
    @Mock
    private MonetaryAmount recipientBalance;
    @Mock
    private MonetaryAmount exchangedTransferAmount;
    private MonetaryAmount transferAmountBeforeExchange;

    @BeforeEach
    public void setUp() {
        transferService = new TransferService(accountRepository, exchangeService);
        transfer = new Transfer(RECIPIENT_ACCOUNT_NUMBER, SENDER_ACCOUNT_NUMBER, BigDecimal.valueOf(50L));
        transferAmountBeforeExchange = getDefaultAmountFactory().setCurrency(SENDER_CURRENCY).setNumber(transfer.getAmount()).create();
        when(accountRepository.findBy(RECIPIENT_ACCOUNT_NUMBER)).thenReturn(recipientAccount);
        when(accountRepository.findBy(RECIPIENT_ACCOUNT_NUMBER)).thenReturn(recipientAccount);
        when(accountRepository.findBy(SENDER_ACCOUNT_NUMBER)).thenReturn(senderAccount);
        when(recipientAccount.getBankBalance()).thenReturn(recipientBalance);
        when(senderAccount.getBankBalance()).thenReturn(senderBalance);
        when(recipientAccount.getCurrency()).thenReturn(RECIPIENT_CURRENCY);
        when(senderAccount.getCurrency()).thenReturn(SENDER_CURRENCY);
        when(exchangeService.exchange(getDefaultAmountFactory().setNumber(BigDecimal.valueOf(50L)).setCurrency(SENDER_CURRENCY).create(), RECIPIENT_CURRENCY))
                .thenReturn(exchangedTransferAmount);
        when(senderAccount.withdraw(transferAmountBeforeExchange)).thenReturn(true);
    }

    @Test
    public void returnsTrueWhenTransferSuccessful() {
        when(senderAccount.withdraw(transferAmountBeforeExchange)).thenReturn(true);
        assertThat(transferService.make(transfer)).isTrue();
    }

    @Test
    public void returnsFalseWhenTryingTransferZeroAmount() {
        assertThat(transferService.make(new Transfer(RECIPIENT_ACCOUNT_NUMBER, SENDER_ACCOUNT_NUMBER, BigDecimal.ZERO))).isFalse();
    }

    @Test
    public void returnsFalseWhenScaleBiggerThanTwo() {
        assertThat(transferService.make(new Transfer(RECIPIENT_ACCOUNT_NUMBER, SENDER_ACCOUNT_NUMBER, BigDecimal.valueOf(54321, 3)))).isFalse();
    }

    @Test
    public void returnsFalseWhenTryingTransferNegativeAmount() {
        assertThat(transferService.make(new Transfer(RECIPIENT_ACCOUNT_NUMBER, SENDER_ACCOUNT_NUMBER, BigDecimal.valueOf(-5L)))).isFalse();
    }

    @Test
    public void returnsFalseWhenTransferFails() {
        when(senderAccount.withdraw(transferAmountBeforeExchange)).thenReturn(false);
        assertThat(transferService.make(transfer)).isFalse();
    }

    @Test
    public void updatesSenderAccount() {
        transferService.make(transfer);
        InOrder inOrder = Mockito.inOrder(senderAccount, accountRepository);
        inOrder.verify(senderAccount).withdraw(transferAmountBeforeExchange);
        inOrder.verify(accountRepository).update(senderAccount, recipientAccount);
    }

    @Test
    public void updatesRecipientAccount() {
        transferService.make(transfer);
        InOrder inOrder = Mockito.inOrder(recipientAccount, accountRepository);
        inOrder.verify(recipientAccount).ante(exchangedTransferAmount);
        inOrder.verify(accountRepository).update(senderAccount, recipientAccount);
    }

    @Test
    public void doesNotUpdatesSenderInRepositoryIfUnableToWithdraw() {
        when(senderAccount.withdraw(transferAmountBeforeExchange)).thenReturn(false);
        transferService.make(transfer);
        verify(accountRepository, never()).update(senderAccount);
    }

    @Test
    public void doesNotUpdatesRecipientInRepositoryIfUnableToWithdra() {
        when(senderAccount.withdraw(transferAmountBeforeExchange)).thenReturn(false);
        transferService.make(transfer);
        verify(accountRepository, never()).update(recipientAccount);
    }

    @Test
    public void returnsFalseWhenSenderAccountDoesNotExist() {
        when(accountRepository.findBy(SENDER_ACCOUNT_NUMBER)).thenReturn(null);
        assertThat(transferService.make(transfer)).isFalse();
    }

    @Test
    public void returnsFalseWhenRecipientAccountDoesNotExist() {
        when(accountRepository.findBy(RECIPIENT_ACCOUNT_NUMBER)).thenReturn(null);
        assertThat(transferService.make(transfer)).isFalse();
    }
}