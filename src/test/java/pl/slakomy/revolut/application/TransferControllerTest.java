package pl.slakomy.revolut.application;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import pl.slakomy.revolut.domain.Transfer;
import pl.slakomy.revolut.infrastructure.lock.LockedExecutionProvider;
import ro.pippo.core.Response;
import ro.pippo.core.route.RouteContext;

import java.math.BigDecimal;
import java.util.function.Supplier;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class TransferControllerTest {
    private TransferController transferController;
    private Transfer transfer;
    @Mock
    private TransferService transferService;
    @Mock
    private RouteContext routeContext;
    @Mock
    private Response response;
    @Mock
    private LockedExecutionProvider lockedExecutionProvider;

    @BeforeEach
    public void setUp() {
        transferController = spy(new TransferController(transferService, lockedExecutionProvider));
        when(lockedExecutionProvider.executeLocked(any(Supplier.class), anyLong(), anyLong())).thenAnswer(invocationOnMock -> ((Supplier) invocationOnMock.getArgument(0)).get());
        transfer = new Transfer(1L, 2L, BigDecimal.valueOf(50L));
        when(transferController.getRouteContext()).thenReturn(routeContext);
        when(routeContext.createEntityFromBody(Transfer.class)).thenReturn(transfer);
        when(routeContext.getResponse()).thenReturn(response);
    }

    @Test
    public void usesLockedExecutionProvider() {
        when(transferService.make(transfer)).thenReturn(true);
        transferController.transfer();
        verify(lockedExecutionProvider).executeLocked(any(Supplier.class), eq(transfer.getRecipientAccountNumber()), eq(transfer.getSenderAccountNumber()));
    }

    @Test
    public void returnsOkWhenTransferSuccessful() {
        when(transferService.make(transfer)).thenReturn(true);
        transferController.transfer();
        verify(response).ok();
    }

    @Test
    public void returnsBadRequestWhenTransferNotParsed() {
        when(routeContext.createEntityFromBody(Transfer.class)).thenReturn(null);
        transferController.transfer();
        verify(response).badRequest();
    }

    @Test
    public void returnsBadRequestWhenRecipientAccountNumberIsMissing() {
        when(routeContext.createEntityFromBody(Transfer.class)).thenReturn(new Transfer(null, 2L, BigDecimal.ONE));
        transferController.transfer();
        verify(response).badRequest();
    }

    @Test
    public void returnsBadRequestWhenSenderAccountNumberIsMissing() {
        when(routeContext.createEntityFromBody(Transfer.class)).thenReturn(new Transfer(1L, null, BigDecimal.ONE));
        transferController.transfer();
        verify(response).badRequest();
    }

    @Test
    public void returnsBadRequestWhenSenderAmountIsMissing() {
        when(routeContext.createEntityFromBody(Transfer.class)).thenReturn(new Transfer(1L, 2L, null));
        transferController.transfer();
        verify(response).badRequest();
    }

    @Test
    public void returnsForbiddenWhenTransferServiceReturnsFalse() {
        when(transferService.make(transfer)).thenReturn(false);
        transferController.transfer();
        verify(response).forbidden();
    }
}