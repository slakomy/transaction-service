package pl.slakomy.revolut.application;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.money.MonetaryAmount;
import javax.money.convert.CurrencyConversion;
import javax.money.convert.ExchangeRateProvider;

import static javax.money.Monetary.getCurrency;
import static javax.money.Monetary.getDefaultAmountFactory;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ExchangeServiceTest {
    private ExchangeService exchangeService;
    private MonetaryAmount initialMonetaryAmount;
    @Mock
    private MonetaryAmount exchangedMonetaryAmount;
    @Mock
    private ExchangeRateProvider exchangeRateProvider;
    @Mock
    private CurrencyConversion currencyConversion;

    @BeforeEach
    public void setUp() {
        exchangeService = new ExchangeService(exchangeRateProvider);
        initialMonetaryAmount = getDefaultAmountFactory().setNumber(200).setCurrency("USD").create();
    }

    @Test
    public void doesNotPerformConversionToTheSameCurrency() {
        assertThat(exchangeService.exchange(initialMonetaryAmount, getCurrency("USD"))).isEqualTo(initialMonetaryAmount);
        verifyZeroInteractions(exchangeRateProvider);
    }

    @Test
    public void performsConversionUsingExchangeRateProvider() {
        when(exchangeRateProvider.getCurrencyConversion(getCurrency("PLN"))).thenReturn(currencyConversion);
        initialMonetaryAmount = mock(MonetaryAmount.class);
        when(initialMonetaryAmount.getCurrency()).thenReturn(getCurrency("USD"));
        when(initialMonetaryAmount.with(currencyConversion)).thenReturn(exchangedMonetaryAmount);
        assertThat(exchangeService.exchange(initialMonetaryAmount, getCurrency("PLN"))).isEqualTo(exchangedMonetaryAmount);

    }
}