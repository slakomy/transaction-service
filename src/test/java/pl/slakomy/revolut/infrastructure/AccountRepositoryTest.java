package pl.slakomy.revolut.infrastructure;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.slakomy.revolut.domain.Account;

import javax.inject.Provider;
import javax.money.MonetaryAmount;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import static javax.money.Monetary.getDefaultAmountFactory;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class AccountRepositoryTest {
    @Mock
    private Provider<EntityManager> entityManagerProvider;
    private static EntityManagerFactory emf;
    private static EntityManager em;
    private AccountRepository repository;

    @BeforeAll
    public static void init() {
        emf = Persistence.createEntityManagerFactory("account-pu");
        em = emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(new Account(1L, getDefaultAmountFactory().setNumber(200L).setCurrency("PLN").create()));
        em.getTransaction().commit();

    }

    @BeforeEach
    public void setUp() {
        when(entityManagerProvider.get()).thenReturn(em);
        repository = new AccountRepository(entityManagerProvider);
    }

    @Test
    public void retrievesAccountFromDatabase() {
        Account account = repository.findBy(1L);
        assertThat(account.getBankBalance())
                .isEqualTo(getDefaultAmountFactory().setNumber(200L).setCurrency("PLN").create());
        assertThat(account.getNumber()).isEqualTo(1L);
    }

    @Test
    public void persistsAccountInDatabase() {
        MonetaryAmount monetaryAmount = getDefaultAmountFactory().setNumber(100L).setCurrency("USD").create();
        repository.save(new Account(5L, monetaryAmount));
        Account account = repository.findBy(5L);
        assertThat(account.getNumber()).isEqualTo(5L);
        assertThat(account.getBankBalance().isEqualTo(monetaryAmount));
    }

    @Test
    public void updatesExistingAccountInDatabase() {
        Account account = repository.findBy(1L);
        account.withdraw(getDefaultAmountFactory().setNumber(50L).setCurrency("PLN").create());
        repository.update(account);
        assertThat(repository.findBy(1L).getBankBalance())
                .isEqualTo(getDefaultAmountFactory().setNumber(150L).setCurrency("PLN").create());
    }

    @AfterAll
    public static void tearDown(){
        em.clear();
        em.close();
        emf.close();
    }
}