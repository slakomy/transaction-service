package pl.slakomy.revolut.infrastructure.lock;

import org.junit.jupiter.api.Test;

import java.util.concurrent.locks.Lock;

import static org.assertj.core.api.Assertions.assertThat;

class UsageTrackedLockTest {

    @Test
    public void initializesLockWithZeroUsageCount() {
        assertThat(new UsageTrackedLock().getUsageCount()).isEqualTo(0);
    }

    @Test
    public void initializesLock() {
        assertThat(new UsageTrackedLock().getLock()).isNotNull().isInstanceOf(Lock.class);
    }

    @Test
    public void incrementsUsageCount() {
        UsageTrackedLock lock = new UsageTrackedLock();
        assertThat(lock.getUsageCount()).isEqualTo(0);
        lock.incrementUsageCount();
        assertThat(lock.getUsageCount()).isEqualTo(1);
        lock.incrementUsageCount();
        assertThat(lock.getUsageCount()).isEqualTo(2);
    }

    @Test
    public void incrementsAndDecrementsUsageCount() {
        UsageTrackedLock lock = new UsageTrackedLock();
        lock.incrementUsageCount();
        lock.incrementUsageCount();
        lock.decrementUsageCount();
        assertThat(lock.getUsageCount()).isEqualTo(1);
        lock.decrementUsageCount();
        assertThat(lock.getUsageCount()).isEqualTo(0);
    }

    @Test
    public void returnsDecrementedValue() {
        UsageTrackedLock lock = new UsageTrackedLock();
        lock.incrementUsageCount();
        assertThat(lock.getUsageCount()).isEqualTo(1);
        assertThat(lock.decrementUsageCount()).isEqualTo(0);
    }
}