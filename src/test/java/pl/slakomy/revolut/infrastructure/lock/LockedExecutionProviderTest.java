package pl.slakomy.revolut.infrastructure.lock;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import java.util.concurrent.locks.Lock;
import java.util.function.Supplier;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class LockedExecutionProviderTest {
    private static final Long ACCOUNT_ONE_NUMBER = 1L;
    private static final Long ACCOUNT_TWO_NUMBER = 5L;
    private static final Long ACCOUNT_THREE_NUBMER = 11L;

    private LockedExecutionProvider lockedExecutionProvider;
    @Mock
    private LocksPool locksPool;
    @Mock
    private Lock accountOneLock;
    @Mock
    private Lock accountTwoLock;
    @Mock
    private Lock accountThreeLock;
    @Mock
    private Supplier supplier;

    @BeforeEach
    public void setUp() {
        lockedExecutionProvider = new LockedExecutionProvider(locksPool);
        when(locksPool.getLock(ACCOUNT_ONE_NUMBER)).thenReturn(accountOneLock);
        when(locksPool.getLock(ACCOUNT_TWO_NUMBER)).thenReturn(accountTwoLock);
        when(locksPool.getLock(ACCOUNT_THREE_NUBMER)).thenReturn(accountThreeLock);
    }

    @Test
    public void returnsValueFromSupplier() {
        Object object = new Object();
        when(supplier.get()).thenReturn(object);
        assertThat(lockedExecutionProvider.executeLocked(supplier, ACCOUNT_ONE_NUMBER)).isEqualTo(object);
    }

    @Test
    public void acquiresLockBeforeCallingSupplier() {
        InOrder inOrder = Mockito.inOrder(accountTwoLock, supplier);
        lockedExecutionProvider.executeLocked(supplier, ACCOUNT_TWO_NUMBER);
        inOrder.verify(accountTwoLock).lock();
        inOrder.verify(supplier).get();
    }

    @Test
    public void returnsLockAfterCallingSupplier() {
        InOrder inOrder = Mockito.inOrder(accountTwoLock, supplier);
        lockedExecutionProvider.executeLocked(supplier, ACCOUNT_TWO_NUMBER);
        inOrder.verify(accountTwoLock).lock();
        inOrder.verify(supplier).get();
        inOrder.verify(accountTwoLock).unlock();
    }

    @Test
    public void acquiresLocksForAllIdsBeforeCallingSupplier() {
        InOrder inOrder = Mockito.inOrder(accountOneLock, accountTwoLock, supplier);
        lockedExecutionProvider.executeLocked(supplier, ACCOUNT_ONE_NUMBER, ACCOUNT_TWO_NUMBER);
        inOrder.verify(accountOneLock).lock();
        inOrder.verify(accountTwoLock).lock();
        inOrder.verify(supplier).get();
        inOrder.verify(accountOneLock).unlock();
        inOrder.verify(accountTwoLock).unlock();
    }

    @Test
    public void acquiresLocksSortedByAccountIds() {
        InOrder inOrder = Mockito.inOrder(accountOneLock, accountTwoLock, accountThreeLock, supplier);
        lockedExecutionProvider.executeLocked(supplier, ACCOUNT_TWO_NUMBER, ACCOUNT_THREE_NUBMER, ACCOUNT_ONE_NUMBER);
        inOrder.verify(accountOneLock).lock();
        inOrder.verify(accountTwoLock).lock();
        inOrder.verify(accountThreeLock).lock();
        inOrder.verify(supplier).get();
    }

    @Test
    public void returnsSupplierValueWhenNoIdProvided() {
        Object object = new Object();
        when(supplier.get()).thenReturn(object);
        assertThat(lockedExecutionProvider.executeLocked(supplier)).isEqualTo(object);
    }


    @Test
    public void marksLockUsageAsCompleteAfterCallingSupplier() {
        InOrder inOrder = Mockito.inOrder(locksPool, supplier);
        lockedExecutionProvider.executeLocked(supplier, ACCOUNT_ONE_NUMBER);
        inOrder.verify(supplier).get();
        inOrder.verify(locksPool).markUsageComplete(ACCOUNT_ONE_NUMBER);
    }

    @Test
    public void marksLockUsageAsCompleteWhenExceptionThrownFromSupplier() {
        when(supplier.get()).thenThrow(new IllegalStateException());
        InOrder inOrder = Mockito.inOrder(locksPool, supplier);
        try {
            lockedExecutionProvider.executeLocked(supplier, ACCOUNT_ONE_NUMBER);
            fail("Should throw Exception");
        } catch (Exception e) {
            inOrder.verify(supplier).get();
            inOrder.verify(locksPool).markUsageComplete(ACCOUNT_ONE_NUMBER);
        }
    }
}

