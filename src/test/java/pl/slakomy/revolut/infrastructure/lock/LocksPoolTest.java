package pl.slakomy.revolut.infrastructure.lock;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.concurrent.locks.Lock;

import static org.assertj.core.api.Assertions.assertThat;

class LocksPoolTest {
    private LocksPool locksPool;

    @BeforeEach
    public void setUp() {
        locksPool = new LocksPool();
    }

    @Test
    public void initializesPoolWithEmptyMap() {
        assertThat(locksPool.getLocks()).isEmpty();
    }

    @Test
    public void createsAndPlacesUsageTrackedLockCountOnMap() {
        locksPool.getLock(1L);
        assertThat(locksPool.getLocks().get(1L)).isNotNull();
    }

    @Test
    public void createsLockUsageCountWithCountEqualOne() {
        locksPool.getLock(55L);
        assertThat(locksPool.getLocks().get(55L).getUsageCount()).isEqualTo(1);
    }

    @Test
    public void returnsTheSameLockForTheSameKey() {
        assertThat(locksPool.getLock(13L)).isEqualTo(locksPool.getLock(13L));
    }

    @Test
    public void incrementsUsageCountOnEachRetrieval() {
        locksPool.getLock(25L);
        assertThat(locksPool.getLocks().get(25L).getUsageCount()).isEqualTo(1);
        locksPool.getLock(25L);
        assertThat(locksPool.getLocks().get(25L).getUsageCount()).isEqualTo(2);
    }

    @Test
    public void decrementsUsageCountWhenUsageCompleteMarked() {
        locksPool.getLock(23L);
        locksPool.getLock(23L);
        assertThat(locksPool.getLocks().get(23L).getUsageCount()).isEqualTo(2);
        locksPool.markUsageComplete(23L);
        assertThat(locksPool.getLocks().get(23L).getUsageCount()).isEqualTo(1);
    }

    @Test
    public void removesLockFromPoolWhenAllUsagesMarkedAsComplete() {
        locksPool.getLock(23L);
        locksPool.getLock(23L);
        assertThat(locksPool.getLocks().get(23L)).isNotNull();
        locksPool.markUsageComplete(23L);
        locksPool.markUsageComplete(23L);
        assertThat(locksPool.getLocks().get(23L)).isNull();
    }

    @Test
    public void createsNewLockWhenPreviousUsageComplete() {
        Lock firstUsageLock = locksPool.getLock(9L);
        locksPool.markUsageComplete(9L);
        assertThat(locksPool.getLock(9L)).isNotEqualTo(firstUsageLock);
    }
}