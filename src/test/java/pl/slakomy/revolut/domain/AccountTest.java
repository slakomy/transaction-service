package pl.slakomy.revolut.domain;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.money.Monetary;

import static javax.money.Monetary.getDefaultAmountFactory;
import static org.assertj.core.api.Assertions.assertThat;

class AccountTest {
    public static final long INITIAL_BALANCE_NUMBER = 50L;
    private Account account;

    @BeforeEach
    public void setUp() {
        account = new Account(1L, getDefaultAmountFactory().setNumber(INITIAL_BALANCE_NUMBER).setCurrency("USD").create());
    }

    @Test
    public void returnsTrueWhenWithdrawSucceeded() {
        assertThat(account.withdraw(getDefaultAmountFactory().setNumber(40).setCurrency("USD").create())).isTrue();
    }

    @Test
    public void updatesAccountBalanceWhenWithdrawing() {
        account.withdraw(getDefaultAmountFactory().setNumber(40).setCurrency("USD").create());
        assertThat(account.getBankBalance()).isEqualTo(getDefaultAmountFactory().setNumber(INITIAL_BALANCE_NUMBER - 40).setCurrency("USD").create());
    }

    @Test
    public void allowsWithdrawalOfAllMoney() {
        assertThat(account.withdraw(getDefaultAmountFactory().setNumber(INITIAL_BALANCE_NUMBER).setCurrency("USD").create())).isTrue();
        assertThat(account.getBankBalance()).isEqualTo(getDefaultAmountFactory().setNumber(0L).setCurrency("USD").create());
    }

    @Test
    public void doesNotAllowWithdrawalMoreThanBalance() {
        assertThat(account.withdraw(getDefaultAmountFactory().setNumber(INITIAL_BALANCE_NUMBER + 1L).setCurrency("USD").create())).isFalse();
    }

    @Test
    public void doesNotUpdateAccountBalanceWhenWithdrawFailed() {
        account.withdraw(getDefaultAmountFactory().setNumber(INITIAL_BALANCE_NUMBER + 1L).setCurrency("USD").create());
        assertThat(account.getBankBalance()).isEqualTo(getDefaultAmountFactory().setNumber(INITIAL_BALANCE_NUMBER).setCurrency("USD").create());
    }

    @Test
    public void updatesAccountBalanceWhenAnteing() {
        account.ante(getDefaultAmountFactory().setNumber(10L).setCurrency("USD").create());
        assertThat(account.getBankBalance()).isEqualTo(getDefaultAmountFactory().setNumber(INITIAL_BALANCE_NUMBER + 10L).setCurrency("USD").create());
    }

    @Test
    public void returnsCurrencyOfUnderlyingAmount() {
        assertThat(account.getCurrency()).isEqualTo(Monetary.getCurrency("USD"));
    }

}